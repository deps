# This file is part of the DEPS/graph-includes package
#
# (c) 2009 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::extractor::java;
use strict;
use warnings;

use File::Spec::Functions qw(catfile);

use base qw(graphincludes::extractor);

use graphincludes::params;

# This is only a preliminary extractor for java dependencies.
# Does not try to do anything with .class

# sub get_default_sysincludes {
#   # since this program does mess wih @INC, we must ask another perl
#   # what it thinks @INC is
#   open INC, 'perl -e \'print join "\n", @INC\' |';
#   my @incs;
#   while (<INC>) {
#     chomp;
#     push @incs, $_;
#   }
#   close INC;

#   return @incs;
# }

sub pattern { '\.java$' }

sub getdeps {
  my $self = shift;
  my ($graph) = @_;

  @ARGV = map {$_->{LABEL}} $graph->get_nodes();
  while (<>) {
    my $dstfile;
    if (m/^\s*import\s+(\S+);/) {
      print STDERR "Found import: $_" if $graphincludes::params::debug;
      my $import = $1;
      $import =~ tr,.,/,;
      $dstfile = $self->locatefile ($import . '.java', @graphincludes::params::inclpath);

    } else {
      next;
    }

    if (defined $dstfile) {
      $graph->record_edge ($ARGV, $dstfile);
    } else {
      $self->record_missed_dep ($ARGV, catfile (split(/\./, $1)) . '.java');
    }
  }
}

1;
