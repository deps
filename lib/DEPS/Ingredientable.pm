# This file is part of the DEPS/graph-includes package
#
# (c) 2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

# DEPS::Ingredientable
#
# A base class for simple hash-based objects that may be the result of
# applying a transform to ingredients

package DEPS::Ingredientable;
use strict;
use warnings;

use Set::Object qw();

# An ingredient is an object from which the current object is built/derived
sub add_ingredients {
  my $self = shift;
  my (@ingredients) = @_;

  $self->{_INGREDIENTS} = new Set::Object
    unless defined $self->{_INGREDIENTS};
  $self->{_INGREDIENTS}->insert(@ingredients);
  return $self;
}

sub ingredients {
  my $self = shift;
  my @return;
  if (defined $self->{_INGREDIENTS}) {
    @return = $self->{_INGREDIENTS}->members();
  }
  @return;
}

sub ingredient_count {
  my $self = shift;
  return $self->{_INGREDIENTS}->size();
}

1;
