# This file is part of the DEPS/graph-includes package
#
# (c) 2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package DEPS::Style::Node::GroupStats;

use warnings;
use strict;

use base qw(DEPS::Style);
use Hash::Util qw(lock_keys unlock_keys);
use Carp qw(croak);

sub new {
  my $class = shift;
  my $self = $class->SUPER::new([],
				[qw(expectednodes expectedeges)],
				@_);

  unlock_keys (%$self);

  # FIXME: sane default values depend on language, and of grouping level
  $self->{expectednodes} = 1 unless defined $self->{expectednodes};
  $self->{expectedeges} = 0 unless defined $self->{expectedeges};

  bless ($self, $class);
  lock_keys (%$self);
  return $self;
}

# FIXME: should be able to specify the level relative to which counting is done

sub apply {
  my $self = shift;
  my ($node, $graphnode, $style) = @_;

  my $nnodes = scalar($node->ingredients);
  my $nedges = 0;
  $nedges = scalar(my @edges=$node->{intraedges}->members())
    if defined $node->{intraedges};
  if ($nnodes != $self->{expectednodes} and $nedges != $self->{expectedeges}) {
    # FIXME: be more friendly
    print STDERR "Warning; overriding extralabel from DEPS::Style::Node::GroupStats"
      if defined $style->{extralabel};
    $style->{extralabel} = '[' . $nnodes . (($nedges != $self->{expectedeges}) ? (':'.$nedges) : '' ) . ']';
  }

  return $style;
}

1;
